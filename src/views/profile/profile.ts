import { Vue, Options } from "vue-class-component";
import { isUserConnected } from "@/modules/utilities/user";
import LoginForm from "@/components/auth/login-form/login-form.vue";
import ProfileAvatar from "@/components/profile-avatar/profile-avatar.vue";
import store from "@/store";

@Options({
  components: {
    LoginForm,
    ProfileAvatar
  }
})
export default class Profile extends Vue {
  private user = store.state.user;
  private isUserConnected = isUserConnected();
  
  private updateUser(): void {
    this.user = store.state.user;
    this.isUserConnected = isUserConnected();
  }
}