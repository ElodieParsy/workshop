import { Vue, Options } from "vue-class-component";
import ChallengeModule from "@/modules/api/challenge";
import { APIResponseType } from "@/modules/api/api";
import MCQ from "@/components/challenges/mcq/mcq.vue";
import Finished from "@/components/challenges/finished/finished.vue";
import Confirm from "@/components/challenges/confirm/confirm.vue";
import UserModule from "@/modules/api/user";
import store from "@/store";

@Options({
  components: {
    MCQ,
    Confirm,
    Finished
  }
})
export default class Home extends Vue {
  private challenge = {} as Challenge;
  private challengeDone = false;
  private endChallengeAnimation = false;
  private challengeSucceeded = true;
  private coinsEarned = 0;
  private counter = 0;

  async created(): Promise<void> {
    if (store.state.user) {
      const req = await UserModule.callAPI("has_done_daily", {id: store.state.user.id});
      if (req.type === APIResponseType.SUCCESS) {
        this.challengeDone = req.data.participation;
        if (this.challengeDone === true) {
          return;
        }
      }
    }
    const res = await ChallengeModule.callAPI("get_daily");
    if (res.type === APIResponseType.SUCCESS) {
      this.challenge = res.data;
    }
  }

  private endChallenge(params: {success: boolean, coinsEarned: number}): void {
    this.challengeSucceeded = params.success;
    this.coinsEarned = params.coinsEarned;
    this.endChallengeAnimation = true;
    this.challengeDone = true;
    this.counter = 0;
    setTimeout(() => {
      this.endChallengeAnimation = false;
    }, 5000);
  }
}