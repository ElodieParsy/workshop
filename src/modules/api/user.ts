import APIModule, { APIResponseType, HTTPMethods } from "./api";

const UserModule = APIModule;

UserModule.registerEndpoint("login", async (params: {email: string, password: string}) => {
  const res = await APIModule.requestAPI(HTTPMethods.POST, "/login", params);
  switch (res.status) {
    case 200: {
      return {
        type: APIResponseType.SUCCESS,
        data: res.data,
        message: "Connexion réussie."
      }
    }
    case 400: {
      return {
        type: APIResponseType.ERROR,
        data: res.data,
        message: "Le connexion a échoué. Les paramètres envoyés semblent être en cause."
      }
    }
    default: {
      return {
        type: APIResponseType.ERROR,
        data: res.data,
        message: "La connexion a échoué."
      }
    }
  }
});

UserModule.registerEndpoint(
  "get",
  async (params: {id: User["id"]}) => {
    const res = await APIModule.requestAPI(HTTPMethods.GET, `/utilisateur/${params.id}`);
    switch (res.status) {
      case 200: {
        return {
          type: APIResponseType.SUCCESS,
          data: res.data,
          message: "Les données ont été récupérées."
        }
      }
      case 400: {
        return {
          type: APIResponseType.ERROR,
          data: res.data,
          message: "Les données n'ont pas pu être récupérées. Les paramètres envoyés semblent être en cause."
        }
      }
      default: {
        return {
          type: APIResponseType.ERROR,
          data: res.data,
          message: "Les données n'ont pas pu être récupérées."
        }
      }
    }
  }
);

UserModule.registerEndpoint(
  "update",
  async (params: {id: User["id"], email: User["email"], firstname: User["firstname"], lastname: User["lastname"], avatarURL: User["avatarURL"]}) => {
    const res = await APIModule.requestAPI(HTTPMethods.PUT, "/utilisateur", params);
    switch (res.status) {
      case 200: {
        return {
          type: APIResponseType.SUCCESS,
          data: res.data,
          message: "Les données ont été enregistrées."
        }
      }
      case 400: {
        return {
          type: APIResponseType.ERROR,
          data: res.data,
          message: "Les données n'ont pas pu être enregistrées. Les paramètres envoyés semblent être en cause."
        }
      }
      default: {
        return {
          type: APIResponseType.ERROR,
          data: res.data,
          message: "Les données n'ont pas pu être enregistrées."
        }
      }
    }
  }
);

UserModule.registerEndpoint("has_done_daily", async (params: {id: User["id"]}) => {
    const res = await APIModule.requestAPI(HTTPMethods.GET, `/confirmChallenge/${params.id}`);
    switch (res.status) {
      case 200: {
        return {
          type: APIResponseType.SUCCESS,
          data: res.data,
          message: "Le challenge a été effectué."
        }
      }
      case 400: {
        return {
          type: APIResponseType.ERROR,
          data: res.data,
          message: "Erreur. Les paramètres envoyés semblent être en cause."
        }
      }
      default: {
        return {
          type: APIResponseType.ERROR,
          data: res.data,
          message: "Erreur."
        }
      }
    }
  }
);

export default UserModule;