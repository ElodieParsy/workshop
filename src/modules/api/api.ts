export enum HTTPMethods {
  POST = "POST",
  GET = "GET",
  PUT = "PUT",
  PATCH = "PATCH",
  DELETE = "DELETE",
}

export enum APIResponseType {
  SUCCESS = "SUCCESS",
  ERROR = "ERROR",
}

const APIURL = "http://10.0.0.4:8000";
const APIEndpoints: Map<string, (params: Record<string, unknown>) => Promise<FormattedAPIResponse<any>>> = new Map;

const APIModule: APIModule = {
  registerEndpoint: (name, callback) => {
    if (APIEndpoints.get(name)) {
      throw new Error(`"${name}" API endpoint already registered.`);
    } else {
      APIEndpoints.set(name, callback);
    }
  },
  requestAPI: async (method, endpoint, params) => {
    let request;
    if (method === HTTPMethods.GET) {
      request = new Request(APIURL + endpoint, {
        method: method,
        // headers: headers,
      });  
    }
    else {
      request = new Request(APIURL + endpoint, {
        method: method,
        // headers: headers,
        body: JSON.stringify(params),
      });  
    }
    let res;
    try {
      res = await fetch(request);
    } catch (err) {
      console.error("API ERROR", err);
    }
    let resData = {};

    if (res) {
      try {
        resData = res ? await res.json() : undefined;
      } catch (err) {
        console.error("API ERROR", err);
      }
    }
    if (res) {
      return {
        status: res.status,
        data: resData,
      };
    } else {
      return {
        status: 503,
        data: resData,
      };
    }
  },
  callAPI: async (endpointName, params) => {
    const ep = APIEndpoints.get(endpointName);
    if (ep) {
      return await ep(params ?? {});
    }
    else {
      throw new Error(`"${endpointName}" API endpoint does not exist.`);
    }
  }
}


export default APIModule