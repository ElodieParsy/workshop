import APIModule, { APIResponseType, HTTPMethods } from "./api";

const LeaderboardModule = APIModule;

LeaderboardModule.registerEndpoint("get_leaderboard", async () => {
  const res = await APIModule.requestAPI(HTTPMethods.GET, "/leaderboard");
  switch (res.status) {
    case 200: {
      return {
        type: APIResponseType.SUCCESS,
        data: res.data,
        message: "Classement récupéré."
      }
    }
    case 400: {
      return {
        type: APIResponseType.ERROR,
        data: res.data,
        message: "Le classement n'a pas pu être récupéré. Les paramètres envoyés semblent être en cause."
      }
    }
    default: {
      return {
        type: APIResponseType.ERROR,
        data: res.data,
        message: "Le classement n'a pas pu être récupéré."
      }
    }
  }
});

export default LeaderboardModule;