import APIModule, { APIResponseType, HTTPMethods } from "./api";

const ChallengeModule = APIModule;

ChallengeModule.registerEndpoint("get_daily", async () => {
  const res = await APIModule.requestAPI(HTTPMethods.GET, "/challenge");
  switch (res.status) {
    case 200: {
      return {
        type: APIResponseType.SUCCESS,
        data: res.data,
        message: "Challenge du jour récupéré."
      }
    }
    default: {
      return {
        type: APIResponseType.ERROR,
        data: res.data,
        message: "Le challenge du jour n'a pas pu être récupéré."
      }
    }
  }
});

ChallengeModule.registerEndpoint("get_mcq", async (params: {id: Challenge["id"]}) => {
  const res = await APIModule.requestAPI(HTTPMethods.GET, `/qcm/${params.id}`);
  switch (res.status) {
    case 200: {
      return {
        type: APIResponseType.SUCCESS,
        data: res.data,
        message: "Détails du challenge récupérés."
      }
    }
    case 400: {
      return {
        type: APIResponseType.ERROR,
        data: res.data,
        message: "Les détails du challenge n'ont pas pu être récupérés. Les paramètres envoyés semblent être en cause."
      }
    }
    default: {
      return {
        type: APIResponseType.ERROR,
        data: res.data,
        message: "Les détails du challenge n'ont pas pu être récupérés."
      }
    }
  }
});

ChallengeModule.registerEndpoint("answer_mcq", async (params: {userId: User["id"], challengeId: MCQChallenge["id"], answerId: number}) => {
  const res = await APIModule.requestAPI(HTTPMethods.POST, "/reponse", params);
  switch (res.status) {
    case 200: {
      return {
        type: APIResponseType.SUCCESS,
        data: res.data,
        message: "Réponse enregistrée."
      }
    }
    case 400: {
      return {
        type: APIResponseType.ERROR,
        data: res.data,
        message: "La réponse n'a pas pu être enregistrée. Les paramètres envoyés semblent être en cause."
      }
    }
    default: {
      return {
        type: APIResponseType.ERROR,
        data: res.data,
        message: "La réponse n'a pas pu être enregistrée."
      }
    }
  }
});

// ChallengeModule.registerEndpoint("confirm", async (params: {userId: User["id"], challengeId: Challenge["id"]}) => {
//   const res = await APIModule.requestAPI(HTTPMethods.POST, "/confirmChallenge");
//   switch (res.status) {
//     case 200: {
//       return {
//         type: APIResponseType.SUCCESS,
//         data: res.data,
//         message: "Challenge validé."
//       }
//     }
//     case 400: {
//       return {
//         type: APIResponseType.ERROR,
//         data: res.data,
//         message: "Le challenge n'a pas pu être validé. Les paramètres envoyés semblent être en cause."
//       }
//     }
//     default: {
//       return {
//         type: APIResponseType.ERROR,
//         data: res.data,
//         message: "Le challenge n'a pas pu être validé."
//       }
//     }
//   }
// });

export default ChallengeModule;