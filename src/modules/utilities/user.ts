import store from "@/store";

export const isUserConnected = (): boolean => {
  const user = store.state.user;
  if (Object.keys(user).length) {
    for (const [key, value] of Object.entries(user)) {
      if (value === undefined) {
        return false;
      }
    }
    return true;
  }
  else {
    return false;
  }
}