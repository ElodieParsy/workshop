import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router"
import Home from "@/views/home/home.vue"
import Profile from "@/views/profile/profile.vue"
import Shop from "@/views/shop/shop.vue"
import Leaderboard from "@/views/leaderboard/leaderboard.vue"

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/shop",
    name: "Shop",
    component: Shop
  },
  {
    path: "/leaderboard",
    name: "Leaderboard",
    component: Leaderboard
  },
  {
    path: "/profile",
    name: "Profile",
    component: Profile
  }
]

const router = createRouter({
  history: createWebHistory(),
  routes
})

export default router
