import { Vue, Options } from "vue-class-component";
import store from "@/store";
import Header from "@/components/header/header.vue";

@Options({
  components: {
    Header
  }
})
export default class App extends Vue {

  created(): void {
    const LSUserId = localStorage.getItem("user_id");
    if (LSUserId) {
      store.dispatch("updateUser", parseInt(LSUserId));
    }

    window.addEventListener("resize", this.setStoreViewport);
    this.setStoreViewport();
  }

  private setStoreViewport(): void {
    const viewport: Viewport = {
      height: window.innerHeight,
      width: window.innerWidth
    };
    store.state.viewport = viewport;
    if (viewport.width > 500) {
      store.state.isMobile = false;
    }
    else {
      store.state.isMobile = true;
    }
  }
}