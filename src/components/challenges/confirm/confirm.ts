import { Vue, Options } from "vue-class-component";
import { Prop } from "vue-property-decorator";
import { APIResponseType } from "@/modules/api/api";
import ChallengeModule from "@/modules/api/challenge";
import store from "@/store";

@Options({
})
export default class Confirm extends Vue {
  @Prop({ default: {} as Challenge }) readonly challenge!: Challenge;

  private async confirm() {
    // const res = await ChallengeModule.callAPI("confirm", {userId: store.state.user.id, challengeId: this.challenge.id});
    // if (res.type === APIResponseType.SUCCESS) {
    // }
    this.$emit("confirm", {success: true, coinsEarned: 100});
  }
}