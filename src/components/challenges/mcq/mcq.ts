import { Vue, Options } from "vue-class-component";
import { Prop } from "vue-property-decorator";
import { APIResponseType } from "@/modules/api/api";
import ChallengeModule from "@/modules/api/challenge";
import store from "@/store";

@Options({
})
export default class MCQ extends Vue {
  @Prop({ default: {} as Challenge }) readonly challenge!: Challenge;

  private MCQChallenge = {} as MCQChallenge;

  async created(): Promise<void> {
    const res = await ChallengeModule.callAPI("get_mcq", {id: this.challenge.id});
    if (res.type === APIResponseType.SUCCESS) {
      this.MCQChallenge = res.data;
    }
  }

  private async answer(answerId: number) {
    const res = await ChallengeModule.callAPI("answer_mcq", {userId: store.state.user.id, challengeId: this.MCQChallenge.id, answerId: answerId});
    if (res.type === APIResponseType.SUCCESS) {
      this.$emit("answer", {success: res.data.success, coinsEarned: this.challenge.points});
    }
  }
}