import { Vue, Options } from "vue-class-component";
import { Prop } from "vue-property-decorator";
import UserModule from "@/modules/api/user";
import store from "@/store";
import { APIResponseType } from "@/modules/api/api";


@Options({
})
export default class ProfileAvatar extends Vue {
  @Prop({ default: false}) readonly own: boolean = false; // set true if it's the current user avatar
  // @Prop({ default: null }) readonly userID: string | null = null; // the current user ID
  @Prop({ default: true }) readonly locked: boolean = true; // whether the image can be changed or not
  @Prop({ default: null }) readonly src: string | null = null; // img src if known
  @Prop({ default: ""   }) readonly initials!: string; // user's initials, as a backup for the src
  @Prop({ default: "xs" }) readonly size!: "xs" | "sm" | "md" | "lg" | "xl"; // avatar size

  // private async setAvatarURL(avatarURL: string): Promise<void> {
  //   console.log(avatarURL)
  //   const res = await UserModule.callAPI(
  //     "update",
  //     {
  //       id: store.state.user.id,
  //       email: store.state.user.email,
  //       firstname: store.state.user.firstname,
  //       lastname: store.state.user.lastname,
  //       avatarURL: store.state.user.avatarURL
  //     }
  //   );
  //   if (res.type === APIResponseType.SUCCESS) {
  //     store.commit("setUser", res.data);
  //     this.$emit("update");
  //   }
  //   else {
  //     throw new Error("Echec de la mise à jour de l'avatar.");
  //   }
  // }

  private get computedSource(): string {
    // if (this.src) {
    //   return this.src
    // }
    // else if (this.own && store.state.user.avatarURL) {
    //   return store.state.user.avatarURL;
    // }
    if (this.initials) {
      return this.generateAvatar(this.initials, "white");
    }
    else return "";
  }

  private generateAvatar(
    text: string,
    foregroundColor: string,
    backup_text?: string
  ): string {
    if (!text && backup_text) {
      text = backup_text;
    }
    if (!text) {
      text = "??";
    }
    if (text == "PD") {
      text = "P";
    }
    if (text.length > 2) {
      text = "??";
    }
    const canvas = document.createElement("canvas");
    const context = canvas.getContext("2d");
  
    function random_item(items: any) {
      return items[Math.floor(Math.random() * items.length)];
    }
    if (context && text) {
      canvas.width = 1000;
      canvas.height = 1000;
  
      // Draw background
      const backgrounds: Record<string, string> = {
        A: "#75b24a",
        B: "#e9223c",
        C: "#347f97",
        D: "#3194d1",
        E: "#c83401",
        F: "#435bde",
        G: "#eace0f",
        H: "#ae014f",
        I: "#0c7bd7",
  
        J: "#75b24a",
        K: "#e9223c",
        L: "#347f97",
        M: "#3194d1",
        N: "#c83401",
        O: "#435bde",
        P: "#eace0f",
        Q: "#ae014f",
        R: "#0c7bd7",
  
        S: "#75b24a",
        T: "#e9223c",
        U: "#347f97",
        V: "#3194d1",
        W: "#c83401",
        X: "#435bde",
        Y: "#eace0f",
        Z: "#ae014f",
  
        "?": "#a0225b",
      };
      const key = text.charAt(0).toUpperCase();
      const color: string = backgrounds[key];
  
      context.fillStyle = color;
      context.fillRect(0, 0, canvas.width, canvas.height);
  
      // Draw text
      context.font = "normal 500px Poppins";
      context.fillStyle = foregroundColor;
      context.textAlign = "center";
      context.textBaseline = "middle";
      context.fillText(text.toUpperCase(), canvas.width / 2, canvas.height / 1.8);

      return canvas.toDataURL("image/png");
    }
    return "";
  }

  private get sizeStyle() {
    switch (this.size) {
      case "sm":
        return {
          width: "60px",
          height: "60px",
        };
      case "md":
        return {
          width: "80px",
          height: "80px",
        };
      case "lg":
        return {
          width: "100px",
          height: "100px",
        };

      case "xl":
        return {
          width: "120px",
          height: "120px",
        };

      case "xs":
      default:
        return {
          width: "47px",
          height: "47px",
        };
    }
  }

}
