import { Vue, Options } from "vue-class-component";
import store from "@/store";
import UserModule from "@/modules/api/user";
import { APIResponseType } from "@/modules/api/api";

@Options({
})
export default class LoginForm extends Vue {
  private email = "";
  private password = "";
  private errorMessage = "";
  private errors = {
    fieldEmpty: "Les champs ne sont pas remplis correctement.",
    requestFailed: "La connexion n'a pas pu être établie"
  }

  private async submit() {
    this.errorMessage = "";
    if (this.email.length === 0 || this.password.length === 0) {
      this.errorMessage = this.errors.fieldEmpty;
      return;
    }
    else {
      const res: FormattedAPIResponse<User> = await UserModule.callAPI("login", {email: this.email, password: this.password});
      if (res.type === APIResponseType.SUCCESS) {
        store.commit("setUser", res.data);
        localStorage.setItem("user_id", store.state.user.id.toString());
        this.$emit("login");
      }
      else {
        this.errorMessage = this.errors.requestFailed;
      }
    }
  }
}