type Challenge = {
  id: number,
  type: string,
  name: string,
  description: string,
  isCollective: boolean,
  points: number
}

type MCQChallenge = {
  id: number,
  question: string,
  answers: Array<{id: number, text: string}>
}
