type APIResponse = {
  status: number,
  data: Record<string, unknown>
}

type FormattedAPIResponse<dataType> = {
  type: APIResponseType,
  data: dataType,
  message: string
}

type APIModule = {
  registerEndpoint: (name: string, callback: (params: any) => Promise<FormattedAPIResponse<any>>) => void; // TODO: type callback as a function that takes a param that can contain any properties
  requestAPI: (method: HTTPMethods, endpoint: string, params?: Record<string, unknown>) => Promise<APIResponse>;
  callAPI: (endpointName: string, params?: Record<string, unknown>) => Promise<FormattedAPIResponse<any>>;
}