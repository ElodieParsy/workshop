type User = {
  id: number,
  email: string,
  firstname: string,
  lastname: string,
  score: number,
  avatarURL: string
}

type Viewport = {
  width: number,
  height: number
}
