import { APIResponseType } from '@/modules/api/api';
import UserModule from '@/modules/api/user';
import { createStore } from 'vuex'

export default createStore({
  state: {
    user: {} as User,
    viewport: {width: 0, height: 0},
    isMobile: true
  },
  mutations: {
    setUser(state, user: User): void {
      
      state.user = user;
    }
  },
  actions: {
    async updateUser(state, userId: User["id"]): Promise<void> {
      const res = await UserModule.callAPI("get", {id: userId});
      if (res.type === APIResponseType.SUCCESS) {
        state.commit("setUser", res.data);
      }
    }
  },
  modules: {
  }
})
