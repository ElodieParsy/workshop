import sys, os, json, datetime, re, mysql.connector, requests,smtplib, ssl
from flask import abort
from os import path
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import ldap
ldap.set_option(ldap.OPT_X_TLS_REQUIRE_CERT, ldap.OPT_X_TLS_NEVER)
##########################################################################
#  Script contenant les fonctions 
##########################################################################


class Functions: 

    def __init__(self):
        self.dc = None
        uri = "ldap://dc01.mspr.local"
        self.dc = ldap.initialize(uri)
        self.dc.set_option(ldap.OPT_REFERRALS, 0)
        

    def objectToString(self,element):
        return json.dumps(element,default=self.jconverter,indent=4, sort_keys=True)

    def jconverter(self, o):
        if isinstance(o, datetime.datetime):
            return o.__str__()  


    def objectToDict(self,obj):
        attrs = dir(obj)
        keys = []
        data = {}
        
        for attr in attrs:
            keys.append(attr)
       
        for key in keys:
            data[key] = getattr(obj,key)
        return data



    
    
    


