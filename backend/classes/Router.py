import os, sys, datetime,json,hashlib,mysql.connector, secrets,hashlib,random
from datetime import timedelta

from flask import Flask,request,Response,abort,session
from classes.Functions import Functions
from flaskext.mysql import MySQL
from threading import Timer

class Router: 
 

    def __init__(self,app=None,db=None,functions=None,ip_ban=None):

        self.app = app        
        self.functions= functions
        self.db = db
        

        """Bloc exécuté après le traitement de chaque requête Http
        -> Modification des entêtes de la réponse pour autoriser les requêtes cross-domaines (CORS) + traçage des requêtes.
        """                                      
        @app.after_request
        def afterEveryRequest(response):  
            # Modification des Headers pour autoriser les attributs spéciaux
            headers = request.headers.get('Access-Control-Request-Headers')
            if headers:
                response.headers['Access-Control-Allow-Headers'] = headers
            
            response.headers['Access-Control-Allow-Origin'] = request.environ.get('HTTP_ORIGIN')                
            response.headers['Access-Control-Allow-Credentials'] = 'true'        
            response.headers['Access-Control-Allow-Methods'] = 'GET, HEAD, POST, OPTIONS, PUT, PATCH, DELETE'  
            return response

       

        


    """
        Définitions des routes 

    """
    def register_routes(self):
        app = self.app    
        db = self.db['cursor']
        db_connection = self.db['connection']
        _ = self.functions
        
    
        # Route d'authentification
        #
        @app.route('/login',methods=["POST"])
        def login():
            utilisateur = {}
            # Récupération des paramètres de la requete
            try:
                data = request.get_json(force=True)   
                user = data['email']
                password = data['password']    
            except Exception as e:
                print(e)
                abort(400)
            
            sql = "SELECT * FROM utilisateur WHERE mail=%s and password=%s;"
            value = (user,password)
            db.execute(sql,value)
            db_connection.commit()
            connection = db.fetchone()

            if connection is None:
                print("Veuillez saisir des identifiants valides")
                abort(400)
            else:
                utilisateur={
                    'id':connection[0],
                    'lastname':connection[1],
                    'firstname':connection[2],
                    "email": connection[4],
                    "score": connection[3],
                    "avatarURL":connection[8]

                }
                session['id'] = connection[0]
                
            return utilisateur, 200 



        #
        # Liste le challenge du jour
        #
        @app.route('/challenge',methods=["GET"])
        def get_challenges():
            challenges = []
            challengeJour = {}
            sql = "SELECT * FROM challenge"
            db.execute(sql)
            result = db.fetchall()

            Date = datetime.date.today()
            jour = str(Date).split("-")
            jour = jour[2] 
            
            for challenge in result:
                challenges.append(challenge)

            calculJour = (int(jour)-1) % 3
            challengeRetour = challenges[calculJour]
            challengeJour = {
                "id":challengeRetour[0],
                "name":challengeRetour[1],
                "description":challengeRetour[2],
                "points":challengeRetour[3],
                "isCollective":challengeRetour[4],
                "type":challengeRetour[5]
            }

            return  _.objectToString(challengeJour),200

        #
        # Liste les questions et réponses du QCM
        #
        @app.route('/qcm/<id>',methods=["GET"])
        def get_question_responses(id):
            qcm = {}
            answer = []
            calculJour = random.randrange(1, 4)
            sql = "SELECT * FROM question WHERE idQuestion =" +  str(calculJour )
            db.execute(sql)
            result = db.fetchone()
            qcm = {
                "id":result[0],
                "question":result[1]
            }
            sql2 = "SELECT * FROM reponse WHERE idQuestion="+ str(qcm["id"])
            db.execute(sql2)
            responses = db.fetchall()

            for response in responses:
                answer.append({
                    "id":response[0],
                    "text":response[1]
                })
            
            qcm["answers"] = answer
        
            return  _.objectToString(qcm),200
          
           


        #
        # Liste tous les utilisateurs
        #
        @app.route('/utilisateur',methods=["GET"])
        def get_utilisateurs():
            utilisateurs = []
            sql = "SELECT * FROM utilisateur order by score DESC "
            db.execute(sql)
            result = db.fetchall()
           
            for utilisateur in result:
                utilisateurs.append(utilisateur)
          
            return  _.objectToString(utilisateurs),200

        
        #
        # Modifie les infos d'un utilisateur
        #
        @app.route('/utilisateur',methods=["PUT"])
        def update_utilisateur():
            utilisateur={}
            # Récupération des paramètres de la requete
            try:
                data = request.get_json(force=True)      
            except Exception as e:
                print(e)
                abort(400)

            sql = "UPDATE utilisateur set nom=%s,prenom =%s,mail=%s,avatar=%s WHERE idUtilisateur=%s" 
            values = (data["lastname"],data["firstname"],data["email"],data["avatarURL"],data["id"])
            print(values)
            db.execute(sql,values)
            db_connection.commit()

            sql2 = "SELECT * FROM utilisateur WHERE idUtilisateur ="+  str(data["id"])
            db.execute(sql2)
            result = db.fetchone()
            utilisateur={
                    'id':result[0],
                    'lastname':result[1],
                    'firstname':result[2],
                    "email": result[4],
                    "score": result[3],
                    "avatarURL":result[8]

                }
        
            return  _.objectToString(utilisateur),200


        #
        # Liste tous les groupes
        #
        @app.route('/groupe',methods=["GET"])
        def get_groupes():
            groupes = []
            sql = "SELECT * FROM groupe"
            db.execute(sql)
            result = db.fetchall()
           
            for groupe in result:
                groupes.append(groupe)
          
            return  _.objectToString(groupes),200

        
        #
        # Retourne le statut de la reponse
        #
        @app.route('/reponse',methods=["POST"])
        def get_verdict_response():
            # Récupération des paramètres de la requete
            try:
                data = request.get_json(force=True)    
            except Exception as e:
                print(e)
                abort(400)

            datetime_str= "13-10-2022"
           
    
            sql = "INSERT INTO confirmChallenge (idChallenge,idUtilisateur,date) VALUES (%s,%s,%s)"
            values = (data['challengeId'], data['userId'], datetime_str)
            db.execute(sql,values)
            db_connection.commit()

            sql2 = "SELECT * FROM reponse WHERE idReponse=" +str(data["answerId"])
            db.execute(sql2)
            result = db.fetchone()
 
            if result[2] != 0:
                sql3 = "SELECT Points FROM challenge WHERE idChallenge ="+str(data['challengeId'])
                db.execute(sql3)
                nbPoint = db.fetchone()
                point = nbPoint[0]
                
                getScore = "SELECT score FROM utilisateur WHERE idUtilisateur=" + str(data['userId'])
                db.execute(getScore)
                score = db.fetchone()
                newScore = score[0] + point

                sql5 = "UPDATE utilisateur set score="+ str(newScore) +" WHERE idUtilisateur="+str(data['userId'])
                db.execute(sql5)

                return {"success":True},200
            else:
                return {"success":False},200
        

         
        #
        # Renvoie les infos d'un utilisateur
        #
        @app.route('/utilisateur/<id>',methods=["GET"])
        def get_specific_utilisateur(id):

            sql = "SELECT * FROM utilisateur WHERE idUtilisateur="+ id+";"
            db.execute(sql)
            user = db.fetchone()
            return  _.objectToString(user),200


        #
        # Renvoie si un utilisateur a effectue ou non le challenge
        #
        @app.route('/confirmChallenge/<id>',methods=["GET"])
        def get_participation(id):

            sql = "SELECT * FROM confirmChallenge WHERE idUtilisateur="+ str(id)+" AND date = '13-10-2022';"
            db.execute(sql)
            user = db.fetchone()
            print(user)

            if user is None:
                return {"participation": False}, 200
            else:
                return  {"participation": True},200
        

        




      
 


        




    