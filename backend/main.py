#!/usr/bin/env python
#-*- coding: utf-8 -*-

from flask import Flask
from classes.Functions import Functions
from classes.Router import Router
from flaskext.mysql import MySQL
from datetime import timedelta



_f = Functions()


app = Flask(__name__)
mysql = MySQL()
app.config['SECRET_KEY'] = 'super_secret_key'
app.config['MYSQL_DATABASE_HOST'] = '10.0.0.4'
app.config['MYSQL_DATABASE_PORT'] = 3306
app.config['MYSQL_DATABASE_USER'] = 'mspr'
app.config['MYSQL_DATABASE_PASSWORD'] = 'Mdp1234!'
app.config['MYSQL_DATABASE_DB'] = 'workshop'
mysql.init_app(app)
connection = mysql.connect()
cursor =connection.cursor()
db = {"connection": connection, "cursor": cursor}


router = Router(app=app, db=db, functions=_f)
router.register_routes()

 

if __name__ == "__main__":
    app.run(
        debug=True, 
        port=8000, 
        host="0.0.0.0"
    )
